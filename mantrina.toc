\contentsline {section}{\numberline {0.1}Preface}{2}{}%
\contentsline {chapter}{Contents}{3}{}%
\contentsline {chapter}{\chapternumberline {1}Pronunciation}{6}{}%
\contentsline {section}{\numberline {1.1}Letters.}{6}{}%
\contentsline {section}{\numberline {1.2}Pronunciation of Latin.}{6}{}%
\contentsline {subsection}{Roman Method.}{6}{}%
\contentsline {subsection}{Continental Method.}{7}{}%
\contentsline {subsubsection}{Vowels:}{7}{}%
\contentsline {subsubsection}{Diphthongs:}{7}{}%
\contentsline {subsubsection}{Consonants:}{7}{}%
\contentsline {section}{\numberline {1.3}Syllables.}{7}{}%
\contentsline {section}{\numberline {1.4}Quantity.}{7}{}%
\contentsline {section}{\numberline {1.5}Accent.}{8}{}%
\contentsline {chapter}{\chapternumberline {2}Attributes of Nouns}{9}{}%
\contentsline {section}{\numberline {2.1}Nouns}{9}{}%
\contentsline {subsection}{Gender}{9}{}%
\contentsline {subsubsection}{Gender of Nouns according to signification}{9}{}%
\contentsline {subsection}{Person}{9}{}%
\contentsline {subsection}{Number}{10}{}%
\contentsline {subsection}{Case}{10}{}%
\contentsline {chapter}{\chapternumberline {3}First Declension}{11}{}%
\contentsline {chapter}{\chapternumberline {4}Second Declension}{12}{}%
\contentsline {chapter}{\chapternumberline {5}Adjectives of the 1st \& 2nd Declensions}{13}{}%
\contentsline {chapter}{\chapternumberline {6}Adjectives of the 1st \& 2nd Declensions. [continued]}{14}{}%
\contentsline {chapter}{\chapternumberline {7}Lesson }{15}{}%
\contentsline {chapter}{\chapternumberline {8}Lesson }{16}{}%
\contentsline {chapter}{\chapternumberline {9}Lesson }{17}{}%
\contentsline {chapter}{\chapternumberline {10}Lesson }{18}{}%
\contentsline {chapter}{\chapternumberline {11}Lesson }{19}{}%
\contentsline {chapter}{\chapternumberline {12}Lesson }{20}{}%
\contentsline {chapter}{\chapternumberline {13}Lesson }{21}{}%
\contentsline {chapter}{\chapternumberline {14}Lesson }{22}{}%
\contentsline {chapter}{\chapternumberline {15}Lesson }{23}{}%
\contentsline {chapter}{\chapternumberline {16}Lesson }{24}{}%
\contentsline {chapter}{\chapternumberline {17}Lesson }{25}{}%
\contentsline {chapter}{\chapternumberline {18}Lesson }{26}{}%
\contentsline {chapter}{\chapternumberline {19}Lesson }{27}{}%
\contentsline {chapter}{\chapternumberline {20}Lesson }{28}{}%
\contentsline {chapter}{\chapternumberline {21}Lesson }{29}{}%
\contentsline {chapter}{\chapternumberline {22}Lesson }{30}{}%
\contentsline {chapter}{\chapternumberline {23}Lesson }{31}{}%
\contentsline {chapter}{\chapternumberline {24}Lesson }{32}{}%
\contentsline {chapter}{\chapternumberline {25}Lesson }{33}{}%
\contentsline {chapter}{\chapternumberline {26}Lesson }{34}{}%
\contentsline {chapter}{\chapternumberline {27}Lesson }{35}{}%
\contentsline {chapter}{\chapternumberline {28}Lesson }{36}{}%
\contentsline {chapter}{\chapternumberline {29}Lesson }{37}{}%
\contentsline {chapter}{\chapternumberline {30}Lesson }{38}{}%
\contentsline {chapter}{\chapternumberline {31}Lesson }{39}{}%
\contentsline {chapter}{\chapternumberline {32}Lesson }{40}{}%
\contentsline {chapter}{\chapternumberline {33}Lesson }{41}{}%
\contentsline {chapter}{\chapternumberline {34}Lesson }{42}{}%
\contentsline {chapter}{\chapternumberline {35}Lesson }{43}{}%
\contentsline {chapter}{\chapternumberline {36}Lesson }{44}{}%
\contentsline {chapter}{\chapternumberline {37}Lesson }{45}{}%
\contentsline {chapter}{\chapternumberline {38}Lesson }{46}{}%
\contentsline {chapter}{\chapternumberline {39}Lesson }{47}{}%
\contentsline {chapter}{\chapternumberline {40}Lesson }{48}{}%
\contentsline {chapter}{\chapternumberline {41}Lesson }{49}{}%
\contentsline {chapter}{\chapternumberline {42}Lesson }{50}{}%
\contentsline {chapter}{\chapternumberline {43}Lesson }{51}{}%
\contentsline {chapter}{\chapternumberline {44}Lesson }{52}{}%
\contentsline {chapter}{\chapternumberline {45}Lesson }{53}{}%
\contentsline {chapter}{\chapternumberline {46}Lesson }{54}{}%
\contentsline {chapter}{\chapternumberline {47}Lesson }{55}{}%
\contentsline {chapter}{\chapternumberline {48}Lesson }{56}{}%
\contentsline {chapter}{\chapternumberline {49}Lesson }{57}{}%
\contentsline {chapter}{\chapternumberline {50}Lesson }{58}{}%
\contentsline {chapter}{\chapternumberline {51}Lesson }{59}{}%
\contentsline {chapter}{\chapternumberline {52}Lesson }{60}{}%
\contentsline {chapter}{\chapternumberline {53}Lesson }{61}{}%
\contentsline {chapter}{\chapternumberline {54}Lesson }{62}{}%
\contentsline {chapter}{\chapternumberline {55}Lesson }{63}{}%
\contentsline {chapter}{\chapternumberline {56}Lesson }{64}{}%
\contentsline {chapter}{\chapternumberline {57}Lesson }{65}{}%
\contentsline {chapter}{\chapternumberline {58}Lesson }{66}{}%
\contentsline {chapter}{\chapternumberline {59}Lesson }{67}{}%
\contentsline {chapter}{\chapternumberline {60}Lesson }{68}{}%
