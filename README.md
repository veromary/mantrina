# Mantrina

A Vintage Latin Primer especially adpated to the missal and breviary by Cora I. Townsend, A.B., (University of Michigan).

The Google scan is fairly low resolution for the size of the type. I'm guessing they were short of money, so the whole work has the feel of stuffing as much content into a small space as possible. The last few pages seem to be missing as the dictionary cuts off in the "p" section. This should be easy to fix by generating a glossary as I did with the Vulgate Latin Course.

## The Preface:
The aim ot this little manual is to increase piety and devotion among Catholics, by rendering a Icnowledge of Latin 
available to a greater number than is at present practicable. 
As every translation, however caretully prepared, falls far 
short of the original. Catholics unable to read Latin lose 
much of the beauty and sublimity of the offices of the Church, 
and are debarred from one of the greatest helps to fervent 
devotion. 

The Mantrina (faithful guide) is not designed to supply 
the place of a Latin grammar, except for the most ordinary 
constructions; but is intended merely as an introduction to 
the Latin language, and as a direct preparation for a thorough 
study bf the Missal and Breviary. These lessons are equally 
well adapted, as a beginning book, to the student hoping to 
become a profound Latin scholar, and to the boy or girl 
whose school days must be limited. 

This book has been divided into sixty lessons, so that, at 
the end of a school term of three months, a diligent pupil 
can begin with profit a study of the Missal or Breviary. By 
means of this book, many can learn to read understandingly 
Mass and Vespers in the language of the Church, who have 
but a short time to spend on the study of Latin, and would 
otherwise be obliged to go through life deprived of a powerful aid to devotion. Most of the words, and a large portion 
of the sentences are taken from the Missal, the Breviary, or 
the Holy Scriptures. The advantage of this is obvious. The 
pupil can immediately put in practice the knowledge of Latin acquired from day to day, and can make much more rapid 
progress when he enters upon the study of the Missal and 
Breviary. When the translation of a quotation is given in 
The Mantrina, the authorized version is followed verbatim. 
Whenever a quotation is left to be rendered by the pupil, the 
words in the vocabulary are translated by the English words 
used in the authorized version. 

With the hope that this little book may accomplish the end 
for which it has been written, it is now submitted to the 
Catholic public. The Author. 


## Installation
I'm planning to format using LaTeX. So to edit and compile your own versions, you'll need to install [TeX and Friends](https://tug.org). I use [TinyTeX](https://yihui.org/tinytex/).

The only package I've had to install so far is memoir.

## Support
There's probably a Github way to contact me here. Otherwise you can use [brandt.id.au](https://www.brandt.id.au) or [kidschant.com](https://www.kidschant.com).

## Roadmap
This is a fair bit of work, and with other projects already much further along, I'm not sure if it's worth my while.

## Contributing
I haven't had much experience with collaborating using Git, but I understand that's what it's made for. having more heads working on this could be amazing.

## Authors and acknowledgment
Thank you to Stella at [Learn Church Latin](https://learnchurchlatin.com), especially for her list of [Textbooks for Learning the Latin of the Divine Office](https://learnchurchlatin.com/2021/01/14/textbooks-for-learning-the-latin-divine-office/).

## License
[Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/)

## Project status
Just beginning.

